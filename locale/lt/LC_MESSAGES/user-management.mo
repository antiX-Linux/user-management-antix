��    )      d  ;   �      �     �  	   �     �     �     �     �     �     �  +        .     C     V  a   h  �   �     �     �  	   �     �     �     �     �     �     �     	  8     "   V  "   y  
   �     �     �     �  
   �  �   �  )   �     �     �          -     J     S  �  b     ]
     q
     �
     �
     �
     �
      �
     �
  6   	     @     \     {  s   �  �        �     
     &     6     >     Q     Z     n  -   �  &   �  Z   �  !   3  #   U     y     �     �     �     �    �  Q        U  !   u      �  $   �     �     �                $          "   	                                                #         %                          '                                           
      )                  &   (         !        Add User Add Users Apply Choose User Close Completely Remove User Completely Remove User? Enter Username First and second password must be identical Items to skip repair No Shell Selected: No User Selected: Notice!
        A user can only be recovered if the user
        has not been completely removed! Notice!
        All of the users files will remain in their 
        home directory, unless completely remove
        is checked. 
        Even so, it is recomended that the
        files be backed up! Password Password Manager Passwords Recover Recover User Remove Remove User Repair User Set User for Automatic Login Set as Default User The list of configs need to be empty or your own configs The password cannot contain spaces The username cannot contain spaces User Login User Password User Removal User Repair User Shell Warning!
        Attempting a user repair may restore more
        programs to original settings than you intend!
        It is highly recommended that you first do a
        backup of all files in your home directory. You MUST be root to use this application! You must choose a user You need to choose a shell You need to enter a password You need to enter a username password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:19+EEST
PO-Revision-Date: 2018-09-24 17:23+0300
Last-Translator: Moo
Language-Team: Lithuanian (http://www.transifex.com/anticapitalista/antix-development/language/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Pridėti naudotoją Pridėti naudotojus Taikyti Pasirinkite naudotoją Užverti Visiškai pašalinti naudotoją Visiškai pašalinti naudotoją? Įveskite naudotojo vardą Pirmas ir antras slaptažodžiai privalo būti vienodi Elementai, kurių netaisyti Nepasirinktas joks apvalkalas: Nepasirinktas joks naudotojas: Pranešimas!
        Naudotojas gali būti atkurtas tik tuo atveju,
        jeigu jis nebuvo visiškai pašalintas! Pranešimas!
        Visi naudotojo failai išliks namų kataloge, 
        nebent yra pažymėtas visiško pašalinimo
        parametras. 
        Tačiau bet kokiu atveju yra rekomenduojama
        pasidaryti failų atsarginę kopiją! Slaptažodis Slaptažodžių tvarkytuvė Slaptažodžiai Atkurti Atkurti naudotoją Šalinti Šalinti naudotoją Pataisyti naudotoją Nustatyti automatinį naudotojo prisijungimą Nustatyti kaip numatytąjį naudotoją Konfigūracijų sąrašas turi būti tuščias arba jame turi būti jūsų konfigūracijos Slaptažodyje negali būti tarpų Naudotojo varde negali būti tarpų Naudotojo prisijungimas Naudotojo slaptažodis Naudotojo šalinimas Naudotojo pataisymas Naudotojo apvalkalas Įspėjimas!
        Bandymas pataisyti naudotoją, gali atkurti
        daugiau programų į pradinius nustatymus,
        negu ketinama!
        Yra primygtinai rekomenduojama, iš pradžių,
        pasidaryti visų savo namų kataloge
        esančių failų atsarginę kopiją. Norėdami naudoti šią programą, PRIVALOTE būti pagrindinis (root) naudotojas! Privalote pasirinkti naudotoją Jūs turite pasirinkti apvalkalą Jūs turite įvesti slaptažodį Jūs turite įvesti naudotojo vardą slaptažodis slaptažodis dar kartą 