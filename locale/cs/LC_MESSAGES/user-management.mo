��    )      d  ;   �      �     �  	   �     �     �     �     �     �     �  +        .     C     V  a   h  �   �     �     �  	   �     �     �     �     �     �     �     	  8     "   V  "   y  
   �     �     �     �  
   �  �   �  )   �     �     �          -     J     S  �  b     J
     ]
     p
     y
     �
     �
     �
     �
     �
  !        '  !   <  ]   ^  �   �     �     �     �     �     �  	   �     �     �  (   �  "     >   2     q  -   �     �     �     �     �       �      1        :     T     i  "   }     �     �                $          "   	                                                #         %                          '                                           
      )                  &   (         !        Add User Add Users Apply Choose User Close Completely Remove User Completely Remove User? Enter Username First and second password must be identical Items to skip repair No Shell Selected: No User Selected: Notice!
        A user can only be recovered if the user
        has not been completely removed! Notice!
        All of the users files will remain in their 
        home directory, unless completely remove
        is checked. 
        Even so, it is recomended that the
        files be backed up! Password Password Manager Passwords Recover Recover User Remove Remove User Repair User Set User for Automatic Login Set as Default User The list of configs need to be empty or your own configs The password cannot contain spaces The username cannot contain spaces User Login User Password User Removal User Repair User Shell Warning!
        Attempting a user repair may restore more
        programs to original settings than you intend!
        It is highly recommended that you first do a
        backup of all files in your home directory. You MUST be root to use this application! You must choose a user You need to choose a shell You need to enter a password You need to enter a username password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:19+EEST
PO-Revision-Date: 2018-09-24 17:22+0300
Last-Translator: joyinko <joyinko@azet.sk>
Language-Team: Czech (http://www.transifex.com/anticapitalista/antix-development/language/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Přidat uživatele Přidat uživatele Použít Zvolit uživatele Zavřít Uživatel kompletně vymazán Kompletně smazat uživatele? Vložte uživatelské jméno Hesla se musí shodovat Položky, které nebudou opraveny Shell nebyl vybrán: Žádný uživatel nebyl vybrán: Upozornění!
        Užvatel může být obnoven pouze
        ak nebyl kompletně smazán! Upozornění!
        Všechny domovské složky uživatelů 
        budou zachovány, pokud nevyberete 
        jejich kompletní smazaní.
        I tak je ale doporučeno soubory zazálohovat! Heslo Správa hesel Hesla Obnovit Obnovit uživatele Odstranit Uživatel vymazán Opravit uživatele Automatické přihlašování uživatele Nastavit předvoleného uživatele Seznam konfliktů ve Vašich nastaveních musí být prázdný Heslo nesmí obsahovat mezery Uživatelské jjméno nesmí obsahovat mezery Příhlášení Uživatelské heslo Odstranění uživatele Opravit uživatele Uživatelský Shell Upozornění!
        Pokus o obnovení může vrátit většinu  
        programů do původního nastavení!
        Důrazně proto doporučujeme nejdříve si udělat
        zálohu všech souborů z Vaší domovské složky. Musíte být root pro spuštění této aplikace! Musíte vybrat uživatele Musíte vybrat shell Musíte zadat heslo Musíte zadat uživatelské jméno Heslo Heslo znovu 