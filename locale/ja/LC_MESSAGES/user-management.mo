��    )      d  ;   �      �     �  	   �     �     �     �     �     �     �  +        .     C     V  a   h  �   �     �     �  	   �     �     �     �     �     �     �     	  8     "   V  "   y  
   �     �     �     �  
   �  �   �  )   �     �     �          -     J     S  �  b     ?
     R
     e
     l
  	   �
  !   �
  $   �
  $   �
  1   �
     *     @  "   U  w   x  �   �     �     �          %     ,     B     I     _  6   u  $   �  Q   �  <   #  ?   `     �     �     �     �           Q     $   p     �  '   �  '   �     �                     $          "   	                                                #         %                          '                                           
      )                  &   (         !        Add User Add Users Apply Choose User Close Completely Remove User Completely Remove User? Enter Username First and second password must be identical Items to skip repair No Shell Selected: No User Selected: Notice!
        A user can only be recovered if the user
        has not been completely removed! Notice!
        All of the users files will remain in their 
        home directory, unless completely remove
        is checked. 
        Even so, it is recomended that the
        files be backed up! Password Password Manager Passwords Recover Recover User Remove Remove User Repair User Set User for Automatic Login Set as Default User The list of configs need to be empty or your own configs The password cannot contain spaces The username cannot contain spaces User Login User Password User Removal User Repair User Shell Warning!
        Attempting a user repair may restore more
        programs to original settings than you intend!
        It is highly recommended that you first do a
        backup of all files in your home directory. You MUST be root to use this application! You must choose a user You need to choose a shell You need to enter a password You need to enter a username password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:19+EEST
PO-Revision-Date: 2018-09-24 17:23+0300
Last-Translator: ふうせん Fu-sen. | BALLOON a.k.a. Fu-sen.
Language-Team: Japanese (http://www.transifex.com/anticapitalista/antix-development/language/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 ユーザー追加 ユーザー追加 適用 ユーザーの選択 閉じる ユーザーを削除しました ユーザーを削除しますか？ ユーザー名を入れて下さい 2つのパスワードが一致していません 復旧を外す項目 シェル未選択:  選択していないユーザー: 通知！
ユーザーが完全に除去されていない場合、
ユーザーは復旧することができます！ 通知！
完全に確認して削除しない限り、
ユーザーのファイルはすべて、
自分のホームディレクトリに残ります。
そうであっても、そのファイルを
バックアップすることをお勧めします！ パスワード パスワード管理 パスワード 復旧 ユーザーの復旧 削除 ユーザーの削除 ユーザーの復旧 自動ログインをおこなうユーザーの設定 デフォルトユーザーの設定 設定リストが空であるか、独自の設定を行う必要があります パスワードにスペースを含む事はできません ユーザー名にスペースを含むことはできません ユーザーログイン ユーザーパスワード ユーザーの削除 ユーザーの復旧 ユーザーシェル 警告！
ユーザーの復旧を行う場合、元の設定に
複数のプログラムを復元する可能性があります！
まずホームディレクトリ内にある
全ファイルのバックアップを行うことを強くおすすめします。 このアプリケーションは root で実行されなけれいけません！ ユーザーを選択して下さい シェルを選択 パスワードを入力して下さい ユーザー名を入力して下さい パスワード パスワード 再度 