��    )      d  ;   �      �     �  	   �     �     �     �     �     �     �  +        .     C     V  a   h  �   �     �     �  	   �     �     �     �     �     �     �     	  8     "   V  "   y  
   �     �     �     �  
   �  �   �  )   �     �     �          -     J     S  +  b     �
     �
     �
     �
  	   �
  "   �
  #        (     C     a     ~  #   �  Y   �  �        �     �     �     �     �               $  #   8  %   \  D   �     �  .   �          0     F     _     u  �   �  #   o     �     �     �  #   �     �                     $          "   	                                                #         %                          '                                           
      )                  &   (         !        Add User Add Users Apply Choose User Close Completely Remove User Completely Remove User? Enter Username First and second password must be identical Items to skip repair No Shell Selected: No User Selected: Notice!
        A user can only be recovered if the user
        has not been completely removed! Notice!
        All of the users files will remain in their 
        home directory, unless completely remove
        is checked. 
        Even so, it is recomended that the
        files be backed up! Password Password Manager Passwords Recover Recover User Remove Remove User Repair User Set User for Automatic Login Set as Default User The list of configs need to be empty or your own configs The password cannot contain spaces The username cannot contain spaces User Login User Password User Removal User Repair User Shell Warning!
        Attempting a user repair may restore more
        programs to original settings than you intend!
        It is highly recommended that you first do a
        backup of all files in your home directory. You MUST be root to use this application! You must choose a user You need to choose a shell You need to enter a password You need to enter a username password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:19+EEST
PO-Revision-Date: 2018-09-24 17:23+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Slovak (http://www.transifex.com/anticapitalista/antix-development/language/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Pridať užívateľa Pridať užívateľov Použiť Vybrať užívateľa Zatvoriť Užívateľ kompletne odstránený Odstrániť užívateľa kompletne? Zadať uživateľské meno Obe heslá sa musia zhodovať Položky ktoré neopravovať Shell nebol vybraný: Nebol vybraný žiadny užívateľ: Upozornenie!
Užívateľ môže byť obnovený len vtedy
ak nebol kompletne odstránený! Upozornenie!
Všetky súbory užívateľa v domovskej zložke
zostanú zachované, pokiaľ nevyberiete voľbu 
kompletné odstránenie.
Aj tak sa ale doporučuje vytvoriť zálohu! Heslo Správa hesiel Heslá Obnoviť Obnoviť užívateľa Vymazať Odstrániť užívateľa Oprava užívateľa Povoliť automatické prihlasovanie Nastaviť ako predvolený užívateľ Zoznam nastavení musí byť prázdny alebo vaše osobné nastavenia Heslo nesmie obsahovať medzery Užívaleľské meno nesmie obsahovať medzery Prihlásenie užívateľa Užívateľské heslo Odstrániť užívateľa Opraviť užívateľa Shell užívateľa Upozornenie!
Pokus o obnovu užívateľa môže obnoviť pôvodné 
nastavenia aj u programov, u kotrých ste to nemali v úmysle!
Dôrazne sa doporučuje najskôr vytvoriť zálohu
všetkých súborov vo Vašej domovskej zložke. Musíte byť prihlásený ako root! Musíte vybrať užívateľa Musíte vybrať shell Musíte zadať heslo Musíte zadať užívateľské meno heslo heslo znovu 