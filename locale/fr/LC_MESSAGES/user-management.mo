��    )      d  ;   �      �     �  	   �     �     �     �     �     �     �  +        .     C     V  a   h  �   �     �     �  	   �     �     �     �     �     �     �     	  8     "   V  "   y  
   �     �     �     �  
   �  �   �  )   �     �     �          -     J     S  �  b     <
     P
  	   e
     o
     �
  %   �
  '   �
     �
  =   �
  !   7  $   Y      ~  a   �  �        �     �     �     �     	  	   #     -     E  4   ]  -   �  I   �  /   
  %   :     `     u     �     �      �  �   �  V   �  !   "  &   D  !   k  &   �     �     �                $          "   	                                                #         %                          '                                           
      )                  &   (         !        Add User Add Users Apply Choose User Close Completely Remove User Completely Remove User? Enter Username First and second password must be identical Items to skip repair No Shell Selected: No User Selected: Notice!
        A user can only be recovered if the user
        has not been completely removed! Notice!
        All of the users files will remain in their 
        home directory, unless completely remove
        is checked. 
        Even so, it is recomended that the
        files be backed up! Password Password Manager Passwords Recover Recover User Remove Remove User Repair User Set User for Automatic Login Set as Default User The list of configs need to be empty or your own configs The password cannot contain spaces The username cannot contain spaces User Login User Password User Removal User Repair User Shell Warning!
        Attempting a user repair may restore more
        programs to original settings than you intend!
        It is highly recommended that you first do a
        backup of all files in your home directory. You MUST be root to use this application! You must choose a user You need to choose a shell You need to enter a password You need to enter a username password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:19+EEST
PO-Revision-Date: 2018-09-24 17:22+0300
Last-Translator: cyril cottet <cyrilusber2001@yahoo.fr>
Language-Team: French (http://www.transifex.com/anticapitalista/antix-development/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Ajouter Utilisateur Ajouter Utilisateurs Appliquer Choisissez l'utilisateur Fermer Supprimer complètement l'utilisateur Supprimer complètement l'utilisateur ? Entrer le nom d'utilisateur Le premier et le second mot de passe doivent être identiques Items pour manquer la réparation Pas d'interface shell sélectionnée Aucun utilisateur sélectionné: Note!
Un utilisateur peut uniquement être récupéré
s'il n'a pas été entièrement supprimé! Note!
Tous les fichiers utilisateurs resteront 
dans leur répertoire personnel, sauf si 
supprimer complètement est coché. 
Dans tous les cas, il est recommandé 
de sauvegarder vos fichiers! Mot de passe Gestionnaire de mots de passe Mots de passe Récupérer Récupérer l'Utilisateur Supprimer Supprimer l'Utilisateur Réparer l'Utilisateur  Choisir un utilisateur pour la connexion automatique Configurer en tant qu'utilisateur par défaut La liste des configurations doit être vide ou vos propres configurations Le mot de passe ne doit pas contenir d'espaces. Le nom ne peut pas contenir d'espaces Nom de l'utilisateur Mot de passe de l'utilisateur Supprimer l'Utilisateur Réparation de l'utilisateur Interface shell de l'utilisateur Attention!
Tenter de réparer l'utilisateur pourrait réinitialiser
plus de programmes que souhaité!
Il est fortement recommandé que vous fassiez d'abord
une sauvegarde de tous les fichiers dans votre répertoire personnel. Vous DEVEZ être connecté en tant qu'administrateur pour utiliser cette application ! Vous devez choisir un utilisateur Vous devez choisir une interface shell Vous devez entrer un mot de passe Vous devez entrer un nom d'utilisateur mot de passe répéter mot de passe 