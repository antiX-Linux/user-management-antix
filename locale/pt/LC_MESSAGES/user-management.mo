��    )      d  ;   �      �     �  	   �     �     �     �     �     �     �  +        .     C     V  a   h  �   �     �     �  	   �     �     �     �     �     �     �     	  8     "   V  "   y  
   �     �     �     �  
   �  �   �  )   �     �     �          -     J     S  �  b     
     3
     J
     R
     f
      o
  !   �
     �
  <   �
          "     <  X   [  �   �     {     �     �  	   �     �     �     �     �  ,   �  )     C   >  !   �  .   �     �     �     �       $   .  �   S  /   .     ^  *   }     �  (   �     �     �                $          "   	                                                #         %                          '                                           
      )                  &   (         !        Add User Add Users Apply Choose User Close Completely Remove User Completely Remove User? Enter Username First and second password must be identical Items to skip repair No Shell Selected: No User Selected: Notice!
        A user can only be recovered if the user
        has not been completely removed! Notice!
        All of the users files will remain in their 
        home directory, unless completely remove
        is checked. 
        Even so, it is recomended that the
        files be backed up! Password Password Manager Passwords Recover Recover User Remove Remove User Repair User Set User for Automatic Login Set as Default User The list of configs need to be empty or your own configs The password cannot contain spaces The username cannot contain spaces User Login User Password User Removal User Repair User Shell Warning!
        Attempting a user repair may restore more
        programs to original settings than you intend!
        It is highly recommended that you first do a
        backup of all files in your home directory. You MUST be root to use this application! You must choose a user You need to choose a shell You need to enter a password You need to enter a username password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:19+EEST
PO-Revision-Date: 2018-09-24 17:23+0300
Last-Translator: ric
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Adicionar Utilizador adicionar Utilizadores Aplicar Escolher Utilizador Encerrar Remover Utilizador Completamente Remover Utilizador Completamente? Introduzir Nome de utilizador A primeira e a segunda entradas de senha têm que ser iguais Itens a não reparar Nenhuma Shell Selecionado Nenhum utilizador seleccionado Alerta!
Um utilizador só pode ser recuperado se
não tiver sido completamente removido! Atenção!
Todos os ficheiros de utilizador serão deixados
no diretório home, a menos que seja marcada a
caixa remover completamente.
Ainda assim, é recomendável fazer uma cópia de segurança!  Senha Gestor de Senhas Senhas  Recuperar Recuperar Utilizador Remover Remover Utilizador  Repara Utilizador Definir Acesso Automático para o Utilizador Estabelecer como Utilizador Pré-definido A lista de configs tem que ser esvasiada das suas próprias configs A senha não pode conter espaços O nome de utilizador não pode conter espaços Acesso do Utilizador Senha de utilizador Remoção de Utilizador Reparação de Utilizador Intermediário (Shell) de Utilizador Aviso!
Tentar uma reparação de utilizador pode levar a que
mais programas sejam restaurados para as definições 
iniciais do que o pretendido. É recomendável guardar
(backup) todos os ficheiros da directoria home. TEM de ser root para executar esta aplicação! Tem que escolher um utilizador Tem que escolher um intermediário (shell) Tem que introduzir uma senha Tem que introduzir um nome de utilizador senha novamente senha 