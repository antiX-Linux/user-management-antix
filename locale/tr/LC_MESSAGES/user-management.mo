��    %      D  5   l      @     A  	   J     T     Z     f     l     �     �  +   �     �     �     �       	             '     4     ;     G     S     p  8   �  "   �  "   �  
                  )  
   5  )   @     j     �     �     �     �     �  �  �     �     �     �     �       !     '   /     W  1   q     �     �     �     �  	   �     �     	     	      	     7	  ,   K	  &   x	  J   �	     �	  %   
     ,
     @
     V
     l
     �
  3   �
     �
     �
        "        ?     F        #            	   %                    $                                      
       "                !                                                                           Add User Add Users Apply Choose User Close Completely Remove User Completely Remove User? Enter Username First and second password must be identical No Shell Selected: No User Selected: Password Password Manager Passwords Recover Recover User Remove Remove User Repair User Set User for Automatic Login Set as Default User The list of configs need to be empty or your own configs The password cannot contain spaces The username cannot contain spaces User Login User Password User Removal User Repair User Shell You MUST be root to use this application! You must choose a user You need to choose a shell You need to enter a password You need to enter a username password password again Project-Id-Version: antix-development
POT-Creation-Date: 2013-07-06 17:19+EEST
PO-Revision-Date: 2018-09-24 17:24+0300
Last-Translator: mahmut özcan <mahmutozcan65@yahoo.com>
Language-Team: Turkish (http://www.transifex.com/anticapitalista/antix-development/language/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Kullanıcı Ekle Kullanıcılar Ekle Uygula Kullanıcı Seç Kapat Kullanıcı Tamamen Kaldırıldı Kullanıcı Tamamen Kaldırılsın mı? Kullanıcı adını girin Birinci ve ikinci parola birbiriyle aynı olmalı Seçilmiş kabuk yok: Seçilmiş Kullanıcı Yok: Parola Parola Yöneticisi Parolalar Kurtarma Kullanıcıyı Kurtar Kaldır Kullanıcıyı Kaldır Kullanıcıyı Onar Kullanıcıyı Otomatik Giriş Olarak Ayarla Öntanımlı Kullanıcı Olarak Ayarla Yapılandırma listesi boş veya kendi yapılandırmalarınız olmalıdır Parola boşluklar içeremez Kullanıcı adı boşluklar içeremez Kullanıcı Girişi Kullanıcı parolası Kullanıcı Kaldırma Kullanıcı Onarımı Kullanıcı Kabuğu Bu uygulamayı kullanmak için root olmalısınız! Bir kullanıcı seçmelisiniz Bir kabuk seçmelisiniz Bir parola girmeniz gerekli Bşr kullanıcı adı girmelisiniz parola Parola tekrarı 